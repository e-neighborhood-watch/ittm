\documentclass{article}
\usepackage[utf8]{inputenc}

\begin{document}
\begin{code}
{-# OPTIONS --safe --without-K --exact-split #-}

module Ordinal where

open import Agda.Primitive

data Ordinal : Set where
  zero : Ordinal
  succ : Ordinal → Ordinal
  ω : Ordinal


ordinalInduction : { ℓ : Level } { P : Ordinal → Set ℓ } → P zero → ( (a : Ordinal) → P a → P (succ a) ) → P ω → ( b : Ordinal) → P b
ordinalInduction z s o zero = z
ordinalInduction z s o (succ x) = s x (ordinalInduction z s o x)
ordinalInduction z s o ω = o

\end{code}
\end{document}
