\documentclass{article}
\usepackage[utf8]{inputenc}

\begin{document}
\begin{code}

{-# OPTIONS --safe --without-K --exact-split #-}

module ITTM where

open import Ordinal
open import Data.Bool
open import Data.Empty
open import Data.Fin as Fin
open import Data.Integer as ℤ
import Data.List as List
open import Data.List using (List)
open import Data.Maybe as Maybe
open import Data.Maybe.Relation.Unary.Any as Any
open import Data.Nat as ℕ
open import Data.Product
open import Data.Sum
open import Data.Unit
import Data.Vec as Vec
open import Data.Vec using (Vec)
open import Function.Base
open import Relation.Binary.PropositionalEquality
open import Relation.Nullary
open import Relation.Nullary.Decidable as Dec


data Direction : Set where
  L R C : Direction


directionToOffset : Direction → ℤ
directionToOffset L = -1ℤ
directionToOffset R = 1ℤ
directionToOffset C = 0ℤ


Alphabet : Set
Alphabet = Fin 2


record Transition (n : ℕ) : Set where
  field
    direction : Direction
    newState : Fin n
    write : Alphabet


record ITTM (n : ℕ) : Set where
  field
    startState : Fin n
    limitState : Fin n
    -- We should probably do something so that this is connected to Alphabet
    transitions : Vec (Vec (Maybe (Transition n)) 2) n


record Configuration (n : ℕ) : Set where
  field
    tape : ℤ → Alphabet
    tapeHead : ℤ
    currentState : Fin n


lookupTransition : { n : ℕ } → ITTM n → Configuration n → Maybe (Transition n)
lookupTransition record { transitions = ts } record { tape = tape ; currentState = currentState ; tapeHead = tapeHead } =
  Vec.lookup (Vec.lookup ts currentState) (tape tapeHead)


newTape : { n : ℕ } → ( c : Configuration n ) → Transition n → ( z : ℤ ) → Dec ( z ≡ Configuration.tapeHead c ) → Alphabet
newTape c t z (no  _) = Configuration.tape c z
newTape c t z (yes _) = Transition.write t


applyTransition : { n : ℕ } → Configuration n → Transition n → Configuration n
applyTransition c t =
  record
    { tape =
      λ z → newTape c t z (z ℤ.≟ Configuration.tapeHead c)
    ; tapeHead =
      Configuration.tapeHead c ℤ.+ directionToOffset (Transition.direction t)
    ; currentState =
      Transition.newState t
    }


nextStep : { n : ℕ } → ITTM n → Configuration n → Maybe (Configuration n)
nextStep m c =
  Maybe.map (applyTransition c) (lookupTransition m c)


makesTransition : {n : ℕ} → Configuration n → Configuration n → Transition n → Set
makesTransition c₀ c₁ t =
  let
    σ₀ = Configuration.tape c₀
    σ₁ = Configuration.tape c₁
    k₀ = Configuration.tapeHead c₀
    k₁ = Configuration.tapeHead c₁
    qⱼ = Configuration.currentState c₁
  in
    ((z : ℤ) → ((σ₀ z ≡ σ₁ z) ⊎ ((z ≡ k₀) × (σ₁ z ≡ Transition.write t))))
    × (k₁ ≡ k₀ ℤ.+ directionToOffset (Transition.direction t))
    × (qⱼ ≡ Transition.newState t)


step : {n : ℕ} → ITTM n → Configuration n → Configuration n → Set
step {n} m c₀ c₁ =
  let
    maybeTransition : Maybe (Transition n)
    maybeTransition = lookupTransition m c₀
  in
    Σ
      (Is-just maybeTransition)
      (λ x → makesTransition c₀ c₁ (to-witness x))


mapJustImpliesJust : {A B : Set} (f : A → B) (a : Maybe A) → Is-just (Maybe.map f a) → Is-just a
mapJustImpliesJust _ (just x) t = just {x = x} tt


stepImpliesTransition : {n : ℕ} (m : ITTM n) (c : Configuration n) ( next : Is-just (nextStep m c) ) → Σ (Is-just (lookupTransition m c)) ( λ t → to-witness next ≡ applyTransition c (to-witness t) )
stepImpliesTransition m c next with lookupTransition m c
stepImpliesTransition  m c (just tt) | just t =
  mapJustImpliesJust (applyTransition c) (just t) (just tt) , refl


nextStepStepsHelper :
  { n : ℕ }
  ( m : ITTM n )
  ( c : Configuration n )
  ( next : Is-just (nextStep m c) )
  → Configuration.tapeHead (to-witness next)
      ≡ Configuration.tapeHead c ℤ.+ directionToOffset (Transition.direction (to-witness (proj₁ (stepImpliesTransition m c next))))
    × Configuration.currentState (to-witness next) ≡ Transition.newState (to-witness (proj₁ (stepImpliesTransition m c next)))
nextStepStepsHelper m c next with lookupTransition m c
nextStepStepsHelper m c (just tt) | just t = refl , refl


tapeHelper :
  { n : ℕ }
  ( m : ITTM n )
  ( c : Configuration n )
  ( next : Is-just (nextStep m c) )
  → (z : ℤ)
  → Configuration.tape c z ≡ Configuration.tape (to-witness next) z
    ⊎ z ≡ Configuration.tapeHead c × Configuration.tape (to-witness next) z ≡ Transition.write (to-witness (proj₁ (stepImpliesTransition m c next)))
tapeHelper m c next z with lookupTransition m c | z ℤ.≟ Configuration.tapeHead c in eq
tapeHelper m c (just tt) z | just t | no  z≢tapeHead rewrite eq = inj₁ refl
tapeHelper m c (just tt) z | just t | yes z≡tapeHead rewrite eq = inj₂ (z≡tapeHead , refl)


nextStepSteps : { n : ℕ } → ( m : ITTM n ) → ( c : Configuration n ) → ( next : Is-just (nextStep m c) ) → step m c (to-witness next)
nextStepSteps m c next =
  proj₁ transitionExists
  , tapeHelper m c next
  , nextStepStepsHelper m c next
  where
    transitionExists = stepImpliesTransition m c next


limsup : {n : ℕ} → (ℕ → Fin n) → Fin n → Set
limsup seq m = (∃[ x ] (( y : ℕ ) → seq (x ℕ.+ y) Fin.≤ m)) × ((x : ℕ) → ∃[ y ] (seq (x ℕ.+ y) ≡ m))


limit : {n : ℕ} → ITTM n → (run : ℕ → Configuration n) → Configuration n → Set
limit m c l =
  ( Configuration.tapeHead l ≡ +0 )
  × ( Configuration.currentState l ≡ ITTM.limitState m )
  × (( z : ℤ ) → limsup ( λ p → Configuration.tape (c p) z) (Configuration.tape l z))


data PartialRun {n : ℕ} (m : ITTM n) : Configuration n -> Set where
  start : PartialRun m
    record
      { tape = λ _ → # 0
      ; tapeHead = +0
      ; currentState = ITTM.startState m
      }
  succStep : {c₀ c₁ : Configuration n} → step m c₀ c₁ → PartialRun m c₀ → PartialRun m c₁
  limitStep : { c : ℕ → Configuration n } { c_ω : Configuration n } → (( x : ℕ ) → step m (c x) (c (ℕ.suc x))) → limit m c c_ω → PartialRun m c_ω


record Run {n : ℕ} (m : ITTM n) : Set where
  field
    {lastConfig} : Configuration n
    partialRun : PartialRun m lastConfig
    halts : Is-nothing (nextStep m lastConfig)

\end{code}
\end{document}
